package banana.orange.arbanana;

import android.app.Fragment;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Dawid on 2016-06-04.
 */
public class PreviewFragment extends Fragment {
    private Camera mCamera;
    Preview preview;
    static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 666;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCamera = getCameraInstance();
        preview = new Preview(getActivity().getApplicationContext(), mCamera);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return new Preview(getActivity().getApplicationContext(), mCamera);
    }

    public static Camera getCameraInstance() {
        Camera c = null;
        Camera.CameraInfo camInfo = new Camera.CameraInfo();
        try {
            for(int i = 0; i < Camera.getNumberOfCameras(); ++i){
                Camera.getCameraInfo(i, camInfo);
                if(camInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                    c = Camera.open(i); // attempt to get a Camera instance
                    Log.d("camera", "camera successfully acquired: " + i);
                }
                Log.d("camera", "Could not find rear camera");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return c; // returns null if camera is unavailable
    }
    public void shutter(){
        if (mCamera != null){
//                    mCamera.takePicture(null, null, preview);
        }
    }
//    }
}
