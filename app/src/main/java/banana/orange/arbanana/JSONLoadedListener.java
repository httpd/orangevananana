package banana.orange.arbanana;

import org.json.JSONException;

/**
 * Created by Sladzio on 6/4/2016.
 */
public interface JSONLoadedListener {
    void onJsonLoaded() throws JSONException;
}
