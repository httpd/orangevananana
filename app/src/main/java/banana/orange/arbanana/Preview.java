package banana.orange.arbanana;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;


import com.googlecode.tesseract.android.PageIterator;
import com.googlecode.tesseract.android.ResultIterator;
import com.googlecode.tesseract.android.TessBaseAPI;

import org.json.JSONException;

class Preview extends SurfaceView implements SurfaceHolder.Callback, Camera.PreviewCallback, TessBaseAPI.ProgressNotifier {
    SurfaceHolder mHolder;
    Camera mCamera;
    private ARView arView;
    TessBaseAPI baseAPI;
    private Handler mHandler;
    private Thread mOCRWorker;
    private BlockingQueue<byte[]> mOCRQueue;
    private Bitmap mPreviewBitmap;
    boolean shoudDo = true;
    boolean shouldDraw = true;
    Preview(Context context, Camera camera) {
        super(context);
        mCamera = camera;
//        Camera.Parameters param = mCamera.getParameters();
//        param.setPreviewFormat(ImageFormat.RGB_565);
//        mCamera.setParameters(param);
        mHolder = getHolder();
        mHolder.addCallback(this);



//        if(! new File( Environment.getDataDirectory().getAbsolutePath() + "tessdata").exists() ){
//            copyAssetFolder(getContext().getAssets(), "tessdata", Environment.getDataDirectory().getAbsolutePath());
//            Log.d("nie_sds", "Preview: " + Environment.getDataDirectory().listFiles().toString() );
        String intStorageDirectory = getContext().getFilesDir().toString();
        File folder = new File(intStorageDirectory + "/" + "tessdata");
        folder.mkdirs();
        File tarfile = new File(folder.getPath() + "/" + "eng.traineddata");
//        if (!tarfile.exists()){
            AssetManager assets = getContext().getAssets();

            try {

                InputStream in = assets.open("tessdata/eng.traineddata");
                OutputStream out = new FileOutputStream(tarfile);

                copyFile(in, out);
                in.close();
                out.flush();
                out.close();


            } catch (IOException e) {
                e.printStackTrace();
            }
//        }

        startOCR();
        Log.d("TAG2", "0. Target file exists: " + Boolean.toString(tarfile.exists())) ;
//
//        InputStream ims = null;
//        try {
//            ims = getContext().getAssets().open("test5.jpg");
//            Bitmap b = BitmapFactory.decodeStream(ims);
//
//            if (tarfile.exists()){
//                TessBaseAPI baseAPI = new TessBaseAPI(this);
//                baseAPI.init(intStorageDirectory + "/", "eng");
//                baseAPI.readConfigFile("digits");
//
//                Camera.Parameters parameters = camera.getParameters();
//                int width = parameters.getPreviewSize().width;
//                int height = parameters.getPreviewSize().height;
//                int previewFormat = parameters.getPreviewFormat();
//                int bytesPerPixel = ImageFormat.getBitsPerPixel(previewFormat);
//
//                baseAPI.setDebug(true);
//
//                baseAPI.setImage(b);
//
//                String recognizedText = baseAPI.getUTF8Text();
//                Log.d("TAG2", "onPreviewFrame: "+ recognizedText);
//                baseAPI.end();
//            }
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        // load image as Drawable

//        }


    }

    public void surfaceCreated(SurfaceHolder holder) {
        // The Surface has been created, now tell the camera where to draw the preview.
        if(holder == null)Log.d("tag", "surface is null");
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.setPreviewCallback(this);
            mCamera.startPreview();
            mCamera.setDisplayOrientation(90);
        } catch (IOException e) {
            Log.d("tag", "Error setting camera preview: " + e.getMessage());
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d("tag", "surfaceDestroyed");
    }



    public void addLabelOnMainThread(final String s){
        Handler mainHandler = new Handler(getContext().getMainLooper());

        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                if (arView == null){arView = ARView.defaultView;}
                arView.clearPriceLabel();
                arView.addPriceLabel(100, 100, 100, 100, s);
            }
        });
    }
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        Log.d("tag", "surfaceChanged");
    }


//    @Override
//    public void onPictureTaken(byte[] bytes, Camera camera) {
//
////    }

    public void startOCR(){
        baseAPI = new TessBaseAPI();
        baseAPI.setDebug(true);
        final String intStorageDirectory = getContext().getFilesDir().toString();
        File tarfile = new File(intStorageDirectory + "/tessdata/eng.traineddata");
        baseAPI.init(intStorageDirectory + "/", "eng");
        baseAPI.setVariable("tessedit_char_whitelist", "1234567890,.");
        mOCRQueue = new LinkedBlockingQueue<byte[]>();

        mHandler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                String text = msg.getData().getString("OcrText");
                float accuracy = msg.getData().getFloat("OcrAccuracy");
                Rect rect = msg.getData().getParcelable("OcrRect");

                if (arView == null){arView = ARView.defaultView;}
                arView.clearPriceLabel();
                arView.addPriceLabel(0, mCamera.getParameters().getPreviewSize().height-50, 100, 100, text);
                try {
                    assert text != null;
                    String st = text.replace(',','.');
                    Double d = Double.parseDouble(st);

                    if (MainActivity.instance != null){
                        try {
                            String s = MainActivity.instance.formatText(d);
                            arView.addPriceLabel((int)(rect.left * 0.8), (int)(rect.top * 0.8), rect.height(), rect.width(), s);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (NumberFormatException exception){
                    Log.d("LOG3", "onPreviewFrame: Cant parse"+ text);

                }


                Log.d("TAG5", "handleMessage: " + text + " acc: " + accuracy);

            }
        };

        mOCRWorker = new Thread(new Runnable() {
            private Object mPauseLock = new Object();

            @Override
            public void run() {
                while(true){
                    while (!shoudDo) {
                        synchronized (this) {
                            try {
                                wait(100);
                            } catch (InterruptedException e) {
                            }
                        }
                    }

                    try {
                        byte[] imageData = mOCRQueue.take();
                        Camera.Parameters p = mCamera.getParameters();

                        mPreviewBitmap = getBitmapImageFromYUV(imageData, p.getPreviewSize().width, p.getPreviewSize().height);
                        final Matrix mtx = new Matrix();
                        mtx.preRotate(90);


                        mPreviewBitmap =  Bitmap.createBitmap(mPreviewBitmap, 0, 0, mPreviewBitmap.getWidth(), mPreviewBitmap.getHeight(), mtx, false);
                        mPreviewBitmap = toGrayscale(mPreviewBitmap);

                        if (mOCRQueue.size() > 3){
                            mOCRQueue.clear();
                        }

                        baseAPI.clear();
                        baseAPI.setImage(mPreviewBitmap);
                        float meanaccuracy = baseAPI.meanConfidence();

                        String recognized = baseAPI.getUTF8Text();

                        String recognizedText = baseAPI.getUTF8Text();
                        final ResultIterator iterator =  baseAPI.getResultIterator();
                        String lastUTF8Text;

                        float lastConfidence = 0;

                        Rect lastRect;
                        Rect bestRect = null;
                        String bestString = null;
                        float bestConfidence = 0;
                        float size = 0;
                        int count = 0;
                        int level = TessBaseAPI.PageIteratorLevel.RIL_WORD;
                        iterator.begin();
//                        ARView arView = ARView.defaultView;
//                        arView.clearPriceLabel();

                        do {
                            lastUTF8Text = iterator.getUTF8Text(level);
                            lastConfidence = iterator.confidence(level);
                            lastRect = iterator.getBoundingRect(level);

                            if (lastConfidence>meanaccuracy && size < lastRect.width() ){
                                bestConfidence = lastConfidence;
                                bestRect = lastRect;
                                bestString = lastUTF8Text;
                                size = lastRect.width();
                            }
                            count++;

                        } while(iterator.next(level));
                        if (bestString!=null ) {
                            updateOCR(bestString, bestConfidence, bestRect);

//                            Log.d("LOG3", "onPreviewFrame: BestString" + bestString);
//                            try {
//                                Double d = Double.parseDouble(bestString);
//
//                                if (MainActivity.instance != null) {
//                                    try {
//                                        String s = MainActivity.instance.formatText(d);
//                                        arView.addPriceLabel(bestRect.left, bestRect.top, bestRect.height(), bestRect.width(), s);
//
//                                    } catch (JSONException e) {
//                                        e.printStackTrace();
//                                    }
//                                }
//                            } catch (NumberFormatException exception) {
//                                Log.d("LOG3", "onPreviewFrame: Cant parse" + bestString);
//
//                            }
                        }



                    } catch (InterruptedException e) {
                        Log.d("TAG5", "mOCRThread  InterruptedException "+e);
                    }
                }
            }
        });
        mOCRWorker.start();
    }
    private void updateOCR(String text, float accuracy, Rect bounds){
        Message m = new Message();
        Bundle b = new Bundle();
        b.putString("OcrText", text);
        b.putFloat("OcrAccuracy", accuracy);
        b.putParcelable("OcrRect", bounds);

        m.setData(b);
        mHandler.sendMessage(m);
    }

    @Override
    public void onPreviewFrame(final byte[] bytes,final Camera camera) {


        if (mOCRQueue != null && bytes != null) {
            if (mOCRQueue.size() > 1){
                mOCRQueue.clear();

            }

            boolean addedToQueue = mOCRQueue.offer(bytes);
//            Log.d("TAG4", "onPreviewFrame addedToQueue " + addedToQueue + " size " + mOCRQueue.size());

        }

    }
//
//        Camera.Parameters parameters = camera.getParameters();
//        int width = parameters.getPreviewSize().width;
//        int height = parameters.getPreviewSize().height;
//
//        YuvImage yuv = new YuvImage(bytes, parameters.getPreviewFormat(), width, height, null);
//
//        if (arView == null){
//            arView = ARView.defaultView;
//        }
//
//        final String intStorageDirectory = getContext().getFilesDir().toString();
//        File tarfile = new File(intStorageDirectory + "/tessdata/eng.traineddata");
//        if (tarfile.exists()){
//
//
//                final TessBaseAPI baseAPI = new TessBaseAPI(Preview.this);
//                baseAPI.setDebug(true);
//
//                baseAPI.init(intStorageDirectory + "/", "eng");
//                baseAPI.readConfigFile("digits");
//                baseAPI.setVariable("tessedit_char_whitelist", "1234567890,.");
//
//                Camera.Parameters parameters = camera.getParameters();
//
//                int width = parameters.getPreviewSize().width;
//                int height = parameters.getPreviewSize().height;
//                YuvImage yuv = new YuvImage(bytes, parameters.getPreviewFormat(), width, height, null);
//
//                ByteArrayOutputStream out = new ByteArrayOutputStream();
//                yuv.compressToJpeg(new Rect(0, 0, width, height), 50, out);
//
//                byte[] data = out.toByteArray();
//
//                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
//                bitmap = toGrayscale(bitmap);
//
//                final Matrix mtx = new Matrix();
//                mtx.preRotate(90);
//
//
//                baseAPI.setImage(Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mtx, false));
//
//                Log.d("TAG3", "onPreviewFrame: yuv");
//
//
//
//                String recognizedText = baseAPI.getUTF8Text();
//                final ResultIterator iterator =  baseAPI.getResultIterator();
//                String lastUTF8Text;
//
//                float lastConfidence = 0;
//
//                Rect lastRect;
//                Rect bestRect = null;
//                String bestString = null;
//                float bestConfidence = 0;
//
//            int count = 0;
//                int level = TessBaseAPI.PageIteratorLevel.RIL_WORD;
//                iterator.begin();
//                ARView arView = ARView.defaultView;
//                arView.clearPriceLabel();
//
//                do {
//                    lastUTF8Text = iterator.getUTF8Text(level);
//                    lastConfidence = iterator.confidence(level);
//                    lastRect = iterator.getBoundingRect(level);
//                    if (lastConfidence>bestConfidence){
//                        bestConfidence = lastConfidence;
//                        bestRect = lastRect;
//                        bestString = lastUTF8Text;
//                    }
//                    count++;
//
//                } while(iterator.next(level));
//                if (bestString!=null ){
//                    Log.d("LOG3", "onPreviewFrame: BestString"+ bestString);
//                    try {
//                        Double d = Double.parseDouble(bestString);
//
//                        if (MainActivity.instance != null){
//                            try {
//                                String s = MainActivity.instance.formatText(d);
//                                arView.addPriceLabel(bestRect.left, bestRect.top, bestRect.height(), bestRect.width(), s);
//
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    } catch (NumberFormatException exception){
//                        Log.d("LOG3", "onPreviewFrame: Cant parse"+ bestString);
//
//                    }
//
//                }
//
//            Log.d("TAG2", "onPreviewFrame: "+ recognizedText);
//    //         shouldDraw = true;
//                baseAPI.end();
//
//        }
//    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        if (action == MotionEvent.ACTION_DOWN){
            shoudDo = !shoudDo;
        }
        return true;
    }

    public Bitmap toGrayscale(Bitmap bmpOriginal)
    {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }
//    public Bitmap RemoveNoise(Bitmap bmap)
//    {
//
//        for (int x = 0; x < bmap.getWidth(); x++)
//        {
//            for (int y = 0; y < bmap.getHeight(); y++)
//            {
//                int pixel = bmap.getPixel(x, y);
//                if ( Color.red(pixel) < 162 && Color.green(pixel) < 162 && Color.blue(pixel) < 162)
////                    bmap.SetPixel(x, y, Color.Black);
//                    bmap.setPixel(x,y, Color.BLACK);
//            }
//        }
//
//        for (int x = 0; x < bmap.getWidth(); x++)
//        {
//            for (int y = 0; y < bmap.getHeight(); y++)
//            {
//                int pixel = bmap.getPixel(x, y);
//                if (Color.red(pixel) > 162 && Color.green(pixel) > 162 && Color.blue(pixel) > 162)
//                    bmap.setPixel(x,y, Color.WHITE);
//            }
//        }
//
//        return bmap;
//    }





    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    @Override
    public void onProgressValues(TessBaseAPI.ProgressValues progressValues) {

        Log.d("TAG3", "onProgressValues: "+progressValues.getPercent());
    }


    public Bitmap getBitmapImageFromYUV(byte[] data, int width,
                                               int height) {
        Camera.Parameters parameters =  mCamera.getParameters();

        YuvImage yuv = new YuvImage(data,  parameters.getPreviewFormat(), width, height, null);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        yuv.compressToJpeg(new Rect(0, 0, width, height), 50, out);

        byte[] bytes = out.toByteArray();

        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

        return bitmap;
    }


}

 class Policz extends AsyncTask<Pair<TessBaseAPI, Bitmap> , Void, TessBaseAPI> {
     @Override
     protected TessBaseAPI doInBackground(Pair<TessBaseAPI, Bitmap>... params) {

         TessBaseAPI baseAPI = params[0].first;
         Bitmap bitmap = params[0].second;


         return baseAPI;
     }

     @Override
     protected void onPostExecute(TessBaseAPI baseAPI) {


     }
 }